<!--
SPDX-FileCopyrightText: 2021 Andrea Laisa <andrea.laisa chiocciola studenti punto unimi dot it>

SPDX-License-Identifier: CC0-1.0
-->

# motore-altaleuico

Progetto di sistemi embedded che consiste nel creare una sorta di motore che giri grazie all'effetto altalena